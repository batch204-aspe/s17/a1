/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

	function personInfo () {

		let fullName = prompt ("What is your name?");
		let age = prompt("What is your age?");
		let location = prompt("Where is your location?");

		console.log("Hello, " + fullName);
		console.log("You are " + age + " years old.");
		console.log("You live in " + location + " City.");

		alert("Thank You");


	};

	personInfo();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function myTopFiveArtist () {
		let myTopArtist = ["Michael Jackson", "Queen", "Owl City", "Taylor Swift", "Imagine Dragons"];

		console.log("1. " + myTopArtist[0]);
		console.log("2. " + myTopArtist[1]);
		console.log("3. " + myTopArtist[2]);
		console.log("4. " + myTopArtist[3]);
		console.log("5. " + myTopArtist[4]);
	};
	myTopFiveArtist();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function myTopFiveMovies(){
		let myFavMovies = ["3 Idiots", "Good Will Hunting",  "Narnia Triology",  "Lord of the Rings Trilogy",  "John Wick Trilogy"];

		console.log("1. " + myFavMovies[0] + "\n" + "Rotten Tomatoes Rating: 99%");
		console.log("2. " + myFavMovies[1] + "\n" + "Rotten Tomatoes Rating: 98%");
		console.log("3. " + myFavMovies[2] + "\n" + "Rotten Tomatoes Rating: 97%");
		console.log("4. " + myFavMovies[3] + "\n" + "Rotten Tomatoes Rating: 96%");
		console.log("5. " + myFavMovies[4] + "\n" + "Rotten Tomatoes Rating: 95%");

	};
	myTopFiveMovies();


/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


	let printFriends = function printUsers(){

	alert("Hi! Please add the names of your friends.");

	let friend1 = prompt("Enter your first friend's name:");
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};
printFriends();

/*console.log(friend1);
console.log(friend2);*/
